import "./styles/App.scss";

import React, { useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import gsap from "gsap";

import Header from "./components/header";
import Navigation from "./components/navigation";

import Home from "./pages/home";
import About from "./pages/about";
import Approach from "./pages/approach";
import Services from "./pages/services";
import CaseStudies from "./pages/caseStudies";

// Routes
const routes = [
    { path: "/", name: "Home", component: Home },
    { path: "/about", name: "About Us", component: About },
    { path: "/approach", name: "Approach", component: Approach },
    { path: "/services", name: "Services", component: Services },
    { path: "/case-studies", name: "Case Studies", component: CaseStudies }
];

function App() {
    useEffect(() => {
        // Mobile viewport height fix
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty("--vh", `${vh}px`);

        // Prevents flashing
        gsap.to("body", 0, { css: { visibility: "visible" } });
    });

    return (
        <>
            <Header />
            <Navigation />
            <div className="App">
                <Switch>
                    {routes.map(({ path, component }) => (
                        <Route
                            key={path}
                            path={path}
                            component={component}
                            exact
                        />
                    ))}
                </Switch>
            </div>
        </>
    );
}

export default App;

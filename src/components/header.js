import React, { useState, useEffect } from "react";
import { withRouter, NavLink } from "react-router-dom";
import { ReactComponent as UpArrow } from "../assets/up-arrow-circle.svg";

import { openMenu, closeMenu } from "../animations/menuAnimations";

const Header = ({ history }) => {
    const [menuState, setMenuState] = useState({ menuIsOpen: false });

    useEffect(() => {
        // Close menu on URL change
        history.listen(() => {
            setMenuState({ menuOpened: false });
        });

        if (menuState.menuIsOpen === true) {
            openMenu();
        } else {
            closeMenu();
        }
    }, [history, menuState]);

    return (
        <div className="header">
            <div className="container">
                <div className="row v-center space-between">
                    <div className="logo">
                        <NavLink to="/">AGENCY.</NavLink>
                    </div>

                    <div className="nav-toggle">
                        <div
                            className="hamburger-menu"
                            onClick={() => setMenuState({ menuIsOpen: true })}
                        >
                            <span></span>
                            <span></span>
                        </div>

                        <div
                            className="hamburger-menu-close"
                            onClick={() => setMenuState({ menuIsOpen: false })}
                        >
                            <UpArrow />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default withRouter(Header);

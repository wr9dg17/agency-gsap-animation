import React, { useState, useEffect } from "react";
import gsap from "gsap";

import Banner from "../components/banner";
import Cases from "../components/cases";
import IntroOverlay from "../components/introOverlay";

const tl = gsap.timeline();

const homeAnimation = callback => {
    tl.from(".line span", 1.8, {
        y: 100,
        ease: "power4.out",
        delay: 0.5,
        skewY: 7,
        stagger: 0.2
    })
        .to(".overlay-top", 1.4, {
            height: 0,
            ease: "expo.inOut",
            stagger: 0.2,
            delay: -0.6
        })
        .to(".overlay-bottom", 1.4, {
            width: 0,
            ease: "expo.inOut",
            stagger: 0.2,
            delay: -0.6
        })
        .to(".intro-overlay", 0, {
            css: { display: "none" }
        })
        .from(".case-image img", 1.6, {
            scale: 1.4,
            ease: "expo.inOut",
            delay: -2,
            stagger: 0.2,
            onComplete: callback
        });
};

const Home = () => {
    const [animationCompleted, setAnimationCompleted] = useState(false);

    const onCompleteHomeAnimation = () => {
        setAnimationCompleted(true);
    };

    useEffect(() => {
        homeAnimation(onCompleteHomeAnimation);
    }, []);

    return (
        <>
            {!animationCompleted ? <IntroOverlay /> : null}
            <Banner />
            <Cases />
        </>
    );
};

export default Home;
